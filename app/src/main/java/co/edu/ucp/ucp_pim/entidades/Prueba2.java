package co.edu.ucp.ucp_pim.entidades;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import org.parceler.Parcel;

import co.edu.ucp.ucp_pim.datos.MyDatabase;

/**
 * Created by raduque on 23/08/2016.
 */
@Table(database = MyDatabase.class)
@Parcel(analyze={Prueba2.class})   // add Parceler annotation here
public class Prueba2 extends BaseModel {


    // empty constructor needed by the Parceler library
    public Prueba2() {
    }


    @Column
    @PrimaryKey
    int id;

    @Column
    private
    String name;

    @Column
    @ForeignKey(saveForeignKeyModel = false)
    Prueba prueba;

    public void setOrganization(Prueba prueba) {
        this.prueba = prueba;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}