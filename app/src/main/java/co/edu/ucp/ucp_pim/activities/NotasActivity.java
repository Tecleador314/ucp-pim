package co.edu.ucp.ucp_pim.activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.edu.ucp.ucp_pim.R;

public class NotasActivity extends Fragment implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_notas, container, false);

        Button button = (Button) v.findViewById(R.id.btnPrueba2);
        button.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {

    }
}
