package co.edu.ucp.ucp_pim.activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import co.edu.ucp.ucp_pim.R;

public class NoticiasActivity extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_noticias, container, false);
        WebView myWebView = (WebView) v.findViewById(R.id.webView);
        myWebView.loadUrl("http://www.ucp.edu.co/2016/08/");
        return v;
    }


}
