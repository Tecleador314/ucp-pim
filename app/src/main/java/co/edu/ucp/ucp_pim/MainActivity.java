package co.edu.ucp.ucp_pim;

import android.content.Intent;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.io.File;

import co.edu.ucp.ucp_pim.activities.TabsActivity;
import co.edu.ucp.ucp_pim.entidades.Prueba;
import co.edu.ucp.ucp_pim.entidades.Prueba2;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FragmentTabHost mTabHost;

    private Button BtnIngresar, BtnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        File database=getApplicationContext().getDatabasePath("db_ucp_pim.db");

        if (!database.exists()) {
            // Database does not exist so copy it from assets here
            //Instanciamos DBFLow en el oncreate
            FlowManager.init(new FlowConfig.Builder(this).build());
            Log.i("Database", "Not Found");
        } else {

            Log.i("Database", "Found");
        }



        setContentView(R.layout.activity_main);
        BtnIngresar = (Button) findViewById(R.id.btnIngresar);
        BtnRegistrar = (Button) findViewById(R.id.btnRegistrar);


        //item2 = new Prueba2();
        //String name = new Select().from(Prueba2.class).where(Prueba2_Table.name.is("CodePath")).querySingle().getName();
        //Toast toast1 = Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT);
        //toast1.show();


    }

    @Override
    protected void onStart() {
        super.onStart();

        Prueba item = new Prueba();
        item = new Select().from(Prueba.class).querySingle();

        if (item !=null) {
            BtnIngresar.setVisibility(View.VISIBLE);
            BtnRegistrar.setVisibility(View.GONE);

            Toast toast1 = Toast.makeText(getApplicationContext(),
                    "Usuario registrado", Toast.LENGTH_SHORT);
            toast1.show();
        } else {
            BtnRegistrar.setVisibility(View.VISIBLE);
            BtnIngresar.setVisibility(View.GONE);

        }
    }


    @Override
    public void onClick(View view) {

        if (BtnRegistrar.getId() == view.getId()) {
            // insert
            Prueba item = new Prueba();
            item.setpId(1);
            item.setpNombre("CodePath");
            item.save();

            // insert
            Prueba2 item2 = new Prueba2();
            item2.setName("John Doe");
            item2.setOrganization(item);
            item2.save();

            Toast toast1 = Toast.makeText(getApplicationContext(),
                    "Se registro el usuario", Toast.LENGTH_SHORT);

            toast1.show();

            onStart();


        } else if (BtnIngresar.getId() == view.getId()) {
            Intent intent = new Intent(MainActivity.this, TabsActivity.class);
            startActivity(intent);
        }

        //List<Prueba> PruebaLista = new Select().from(Prueba.class).queryList();


        /*item2.delete();
        item.delete();*/

        /*item = new Select().from(Prueba.class).querySingle();*/
        //List<Prueba2> Prueba2Lista = new Select().from(Prueba2.class).where(Prueba2_Table.name.is("CodePath")).queryList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String mensaje = "";
        Toast toast1;
        switch (item.getItemId()) {

            case R.id.imnuSincronizar:
                mensaje = "Este boton sincroniza con la base de datos";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuConfiguracion:
                mensaje = "Este boton me redirecciona al activity configuración";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuMiUniversidad:

                mensaje = "Este boton me redirecciona al activity Mi Universidad";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuFaq:

                mensaje = "Este boton me mostrará el FAQ y permitirá al usuario preguntar";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuAcercaDe:
                mensaje = "Este boton muestra información sobre el aplicativo(Versión, desarrolladores, personas que colaboraron...)";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuAyuda:
                //metodoSettings()
                mensaje = "Este boton redirecciona a un formulario donde se podrá enviar un correo preguntando algo o dando una sugerencia";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
