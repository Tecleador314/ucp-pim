package co.edu.ucp.ucp_pim.utilidades;

/**
 * Created by raduque on 22/08/2016.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import co.edu.ucp.ucp_pim.R;

public class FragmentTab extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        this.setContentView(R.layout.main);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_layout, container, false);
//        TextView tv = (TextView) v.findViewById(R.id.text);
//        tv.setText(this.getTag() + " Content");

        WebView myWebView = (WebView) v.findViewById(R.id.webView);
        myWebView.loadUrl("http://www.ucp.edu.co/2016/08/");

        return v;
    }
}