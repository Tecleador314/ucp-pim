package co.edu.ucp.ucp_pim.datos;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by raduque on 23/08/2016.
 */
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {

    public static final String NAME = "db_ucp_pim";

    public static final int VERSION = 1;
}