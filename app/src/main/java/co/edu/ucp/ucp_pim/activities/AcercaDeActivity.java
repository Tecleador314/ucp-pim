package co.edu.ucp.ucp_pim.activities;

import android.content.Intent;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import co.edu.ucp.ucp_pim.R;
import co.edu.ucp.ucp_pim.utilidades.FragmentTab;

public class AcercaDeActivity extends AppCompatActivity {

    private FragmentTabHost mTabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acercade);

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(
                mTabHost.newTabSpec("tab1").setIndicator("Horarios", null),
                HorariosActivity.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("tab2").setIndicator("Notas", null),
                FragmentTab.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("tab3").setIndicator("Noticias", null),
                FragmentTab.class, null);

    }


    



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String mensaje = "";
        Toast toast1;
        switch (item.getItemId()) {

            case R.id.imnuSincronizar:
                mensaje = "Este boton sincroniza con la base de datos";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuConfiguracion:
                mensaje = "Este boton me redirecciona al activity configuración";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuMiUniversidad:

                mensaje = "Este boton me redirecciona al activity Mi Universidad";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuFaq:

                mensaje = "Este boton me mostrará el FAQ y permitirá al usuario preguntar";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuAcercaDe:
                mensaje = "Este boton muestra información sobre el aplicativo(Versión, desarrolladores, personas que colaboraron...)";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            case R.id.imnuAyuda:
                //metodoSettings()
                mensaje = "Este boton redirecciona a un formulario donde se podrá enviar un correo preguntando algo o dando una sugerencia";
                toast1 = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





}
