package co.edu.ucp.ucp_pim.entidades;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import org.parceler.Parcel;

import co.edu.ucp.ucp_pim.datos.MyDatabase;

/**
 * Created by raduque on 19/08/2016.
 */
@Table(database = MyDatabase.class)
@Parcel(analyze={Prueba2.class})   // add Parceler annotation here
public class Prueba extends BaseModel {

    // empty constructor needed by the Parceler library
    public Prueba() {
    }


    @Column
    @PrimaryKey
    int pId;

    @Column
    String pNombre;

    public void setpId(int pId) {
        this.pId = pId;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getpNombre() {
        return pNombre;
    }

//
//
//    public int getpId() {
//        return pId;
//    }
//
//    public void setpId(int pId) {
//        this.pId = pId;
//    }
//
//    public String getpNombre() {
//        return pNombre;
//    }
//
//    public void setpNombre(String pNombre) {
//        this.pNombre = pNombre;
//    }
}
