package co.edu.ucp.ucp_pim.activities;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import co.edu.ucp.ucp_pim.R;

public class HorariosActivity extends Fragment implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_horarios);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_horarios, container, false);

        Button button = (Button) v.findViewById(R.id.btnPrueba);
        button.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {

        String a="";
        Toast toast1 = Toast.makeText(getActivity().getApplicationContext(), "Prueba de este boton", Toast.LENGTH_SHORT);
       toast1.show();

    }
}
